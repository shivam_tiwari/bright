<div class="mass-head inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						<h1>Products <small></small></h1>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
						<ol class="breadcrumb">
							<li><a ui-sref="home" ui-sref-active="active">Home</a></li>
							
							<li ui-sref="products" ui-sref-active="active">Products</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

 <div class="headerWrapper">
    <div class="headerImage">
        <img width="1270" style = height:350px ; src="assets/images/analysis-coffee-cup-1549702.jpg"  class="img-responsive" / >
        </div>
        </div>

				
		<div class="block-s2">
			<div class="container">
				<div >

					
				<?php	
				if ( isset($_GET['success']) && $_GET['success'] == 1 )
				{
				     // treat the succes case ex:
				     echo 			die("<h4 style='color:#089de3; font-size: 25px;'>Thank you for the request, Our team will contact you as soon as posible.</h4>");

				} ?>
		
					<p>Our retail point of sale software solutions are designed to address the challenges of small & independent aspirants, retailers and large multi-store retail chains. their sector specific orientation, built using latest technologies, assure retailers 'a visible improvement' in customer engagement, by helping them source the right product at right price at the right time.</p>
				

				</div>

				<!--Pricing Tables 1 -->
					<div class="row block-s3">
					    <a href="retail.php">
					        <div class="col-lg-3 col-sm-6 ">
						
    					    	<div class="pricing-table-1 cls-img-bg" style="background-image: url(assets/images/bar-bottles-commerce-2079438.jpg);width: 100%;background-size: cover;">
                                   <div style="background-color: rgba(0,0,0,0.25); width: 100%; height: 400px;">
    
    						       	<h3 class="plan-title" style="font-size:50px; color: white; font-family: Times New Roman;"><br>Bright<br> Retail</h3>
    							<!-- <span class="bigger-500" style="font-size:32px">-</span> -->
    							    <ul class="plan-features">
    								
    								
    							    	<li><strong></strong>  </li>
    						    	</ul>
    						   	<a href="retail.php" class="btn btn-primary plan-button"><i class="fa fa-download"></i>Read More</a>
    						</div>
    						</div>
    					</div>
    					</a>

					<a href="garment.php">
						<div class="col-lg-3 col-sm-6">
							<div class="pricing-table-1 cls-img-bg" style="background-image: url(assets/images/apparel-attire-blur-994517.jpg);width: 100%;background-size: cover;">
							 	<div style="background-color: rgba(0,0,0,0.25); width: 100%; height: 400px;">
									<h3 class="plan-title" style="font-size:50px; color: white; font-family: Times New Roman;"><br>Bright<br>Garment</h3>
								<!-- <span class="bigger-500" style="font-size:32px"></span> -->
								<ul class="plan-features">	
									<li><strong> </strong>  </li>
								</ul>
								<a href="garment.php" class="btn btn-primary plan-button"><i class="fa fa-download"></i>Read More</a>
								</div>
							</div>
						</div>
					</a>

					<a href="distributor.php">
						<div class="col-lg-3 col-sm-6">
							<div class="pricing-table-1 cls-img-bg" style="background-image: url(assets/images/factory-men-shelves-1797428.jpg);width: 100%;background-size: cover;">
								<div style="background-color: rgba(0,0,0,0.25); width: 100%; height: 400px;">
								<h3 class="plan-title" style="font-size:45px; color: white; font-family: Times New Roman;"><br>Bright<br>Distributors</h3>
								<!-- <span class="bigger-500" style="font-size:32px">RS.15000/-</span> -->
								<ul class="plan-features">
									<li><strong></strong> </li>
								</ul>
								<a href="distributor.php" class="btn btn-primary plan-button"><i class="fa fa-download"></i>Read More</a>
								
							</div>
						</div>
					</div>
					</a>
					<a href="wholesale.php">
					<div class="col-lg-3 col-sm-6">
						<!-- <div class="pricing-table-1 bg-primary"> -->
							<div class="pricing-table-1 cls-img-bg" style="background-image: url(assets/images/asian-assorted-background-917371.jpg);width: 100%;background-size: cover;">
								<div style="background-color: rgba(0,0,0,0.25); width: 100%; height: 400px;">
								
							<h3 class="plan-title" style="font-size:45px; color: white; font-family: Times New Roman;"><br>Bright<br>Wholesale </h3>
							<!-- 
								<span class="bigger-500" style="font-size:32px"></span> -->
							
								<ul class="plan-features">
								
								<li><strong></strong></li>
							</ul>
							
							<a href="wholesale.php" class="btn  plan-button"><i class="fa fa-download"></i>Read More</a>
						</div>
					</div>
					</div>
				</a>

					<form role="form" action="n-curl.php" method="get"  >
						<div class="form-group col-lg-4" >
                	        <label class="control-label required">
                                For Demo Input Your Contact Number</label>
                                <div class="clearfix">
                                    <input type="number"  id="phone" name="phone" placeholder="Mobile number" class="form-control" minlength="10" maxlength="12" required>
                                </div>
        				</div>


        				<div class="form-group col-lg-12">
								 <!--<input type="submit"  value="submit"> -->
							<button type="submit" value="submit"  onclick="submitted=true" isDisabled="isDisabled"  class="btn btn-primary"><i class="fa fa-send"></i>Submit <!-- <span><i class="fa fa fa-spinner fa-spin"></i></span> -->
							</button>
						</div>
					</form>

				</div>
				
				<!--Pricing Tables 1-->
			</div>
		</div>


		