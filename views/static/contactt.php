
<div class="mass-head inner">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
				<h1>Contact Us <small>We'd Love to Hear From You!</small></h1>
			</div>

			<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
				<ol class="breadcrumb">
					<li><a ui-sref="home" ui-sref-active="active">Home</a></li>
					<li ui-sref="contact" ui-sref-active="active">Contact us</li>
				</ol>
			</div>
		</div>
	</div>
</div>

 <div class="headerWrapper">
             <div class="headerImage">
                <img width="1270" style = height:300px ;  src="assets/images/connection-contact-focus-955081 (1).jpg" class="img-responsive" / >
             </div>
        </div>
<div class="container">
	<div class="block-s2">
	<!--CONTENT-->
		<!--Contact Us from-->
		<div class="row">

			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12" ng-if="!formSubmitted">

				<?php	
				if ( isset($_GET['success']) && $_GET['success'] == 1 )
				{
				     // treat the succes case ex:
				     echo 			die("<h4 style='color:#089de3'>Thank you for writing us. Our team will reply you within 48 hours.</h4>");

				} ?>
				
				<p>To get a detailed overview of our products and services in detail, please fill up the Contact Form below:</p>

				<div class="hr-12 hr-double hr-dotted"></div>


				<form role="form" action="email.php" id="gmail.php" method="post" name="contactForm" onsubmit="contactMe()" novalidate>

					<div class="well light">

						<p style="color:red" class="errorMessage"></p>
						<div class="form-group row">

							<div class="form-group col-lg-4" >
								<label class="control-label required">Name</label>
								<div class="clearfix">
									<input type="text" id="name" name="name" min-length="3"  class="form-control" required>
								</div>
								<!-- <div ng-messages="contactForm.name.$error"  for="name" class="help-block">
                                    <p ng-message="required">This field is required.</p>
                                    <p ng-message="min-length">Value is too short.</p>
                                </div> -->
							</div>
							<div class="form-group col-lg-4" >
                                <label class="control-label">
                                Email Address</label>
                                <div class="clearfix">
                                    <input type="email" autocomplete="off" id="email" name="email"  class="form-control" required >
                                </div>
                                <!-- <div ng-messages="contactForm.email.$error" ng-if=" contactForm.email.$invalid && (contactForm.email.$touched || submitted)" for="email" class="help-block">
                                    <p ng-message="email">Please enter correct Email address.</p>
                                </div> -->
                            </div>
                            <div class="form-group col-lg-4" >
                                <label class="control-label required">
                                Phone Number</label>
                                <div class="clearfix">
                                    <input type="number" autocomplete="off" id="phone" name="phone"  class="form-control" minlength="10" maxlength="12" required>
                                </div>
                               <!--  <div ng-messages="contactForm.phone.$error" ng-if=" contactForm.phone.$invalid && (contactForm.phone.$touched || submitted)" for="phone" class="help-block">
                                	<p ng-message="required">This field is required.</p>
                                    <p ng-message="minlength">Please enter correct phone number.</p>
                                    <p ng-message="maxlength">Please enter correct phone number.</p>
                                </div> -->
                            </div>
							<div class="clearfix"></div>
		  					<div class="form-group col-lg-12" >
                                <label class="control-label required">
                                Message</label>
                                <div class="clearfix">
                                    <textarea id="message" name="message"  class="form-control" rows="10" minlength="10" required></textarea>
                                </div>
                                <!-- <div ng-messages="contactForm.message.$error" ng-if="contactForm.message.$invalid && (contactForm.message.$touched || submitted)" for="message" class="help-block">
                                	<p ng-message="required">This field is required.</p>
                                	<p ng-message="minlength">Message is too short.</p>
                                </div> -->
                            </div>
							<div class="form-group col-lg-12">
								<!-- <input type="submit"  value="submit"> -->
								<button onclick="setTimeout(myFunction(),3000);" type="submit" value="submit"  onclick="submitted=true" isDisabled="isDisabled" class="btn btn-primary"><i class="fa fa-send"></i>Submit <!-- <span><i class="fa fa fa-spinner fa-spin"></i></span> --></button>


								<p id="demo"></p>

								<script>
								function myFunction() {
								  document.getElementById("demo").innerHTML = "Thank You";
								}
								</script>
							</div>
						</div>
					</div>
				 </form>
			</div>


			<!-- 

				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12" ng-if="formSubmitted">
					<h4 style="color:#089de3">Thank you for writing us. Our team will reply you within 48 hours.</h4>
				</div> -->
				
				<!-- <iframe width="30%" height="300" src="https://maps.google.com/maps?width=100%&height=600&hl=en&q=bright%20solutions%2Ctarapur%20road%2Cboisar%20west+(Your%20Business%20Name)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/en/journey-planner.htm">Map Directions</a></iframe>				<!--//address-->

		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"  >			
 
				<!--address-->
				<br>
				<h3>Bright Solutions</h3>
				<h4>We Provide Better Solutions</h4>

				<p>207, 2nd floor, Gosaliya Park, <br>Tarapur Road, Near ST Depot, <br>Boisar - 401501, <br>Dist. Palghar, State - Maharashtra, <br>India</p>
				<p><i class="fa fa-phone"></i> <abbr title="Phone">P</abbr>: (+91) 9271-920-690</p>
				<p><i class="fa fa-envelope-o"></i> <abbr title="Email">E</abbr>: <a href="mailto:enquiry@brightsolution.co.in">enquiry@brightsolution.co.in</a></p>
				<p><i class="fa fa-clock-o"></i> <abbr title="Hours">H</abbr>: Monday - Saturday: 10:00 AM to 6:00 PM</p>

				<ul class="list-unstyled list-inline">
					<li class="no-padding-left">
						<a target="_blank" href="https://www.facebook.com/brightsolution.in" class="btn btn-facebook btn-xs btn-circle" data-toggle="tooltip" data-rel="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook icon-only bigger-110"></i></a>
					</li>
					<li class="no-padding-left">
						<a target="_blank" href="https://twitter.com/bri8solutions" class="btn btn-twitter btn-xs btn-circle" data-toggle="tooltip" data-rel="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter icon-only bigger-110"></i></a>
					</li>
					<li class="no-padding-left">
						<a target="_blank" href="https://www.linkedin.com/in/bright-solution-251543143" class="btn btn-linkedin btn-circle btn-xs" data-toggle="tooltip" data-rel="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin icon-only bigger-110"></i></a>
					</li>
				</ul>

		
</div>

		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="width: 100%" >			
					<iframe width="100%" height="400" src="https://maps.google.com/maps?width=100%&height=600&hl=en&q=bright%20solutions%2Ctarapur%20road%2Cboisar%20west+(Your%20Business%20Name)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/en/journey-planner.htm">Map Directions</a></iframe>	
			</div>
</div>
		<div class="row">
			<div class="col-lg-12">
				<h4><i class="fa fa-warning text-yellow"></i> Important note</h4>
				<div class="well light">
					Kindly, add your message or requirement in the above message box in detail. Our customer care executives will be in touch with you within 48 hours.
				</div>
			</div>
		</div>
		<!--//Contact Us from-->

	<!--CONTENT-->
	</div>
</div>

