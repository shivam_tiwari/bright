<?php
//header("Content-type: application/javascript");
ob_start();//compress());

    function compress( $minify )
    {
    /* remove comments */
        $minify = preg_replace( '!/*[^*]**+([^/][^*]**+)*/!', '', $minify );

        /* remove tabs, spaces, newlines, etc. */
        $minify = str_replace( array("rn", "r", "n", "t", '  ', '    ', '    '), '', $minify );

        return $minify;
    }
    /* css files for combining */
    
    include('jquery.min.js');
    include('bootstrap.min.js');
    include('plugins/slimscroll/jquery.slimscroll.min.js');
    include('plugins/pace/pace.min.js');
    include('plugins/rs-slider/rs-plugin/js/jquery.themepunch.tools.min.js');
    include('plugins/rs-slider/rs-plugin/js/jquery.themepunch.revolution.min.js');
    //include('plugins/owl-carousel/owl.carousel.min.js');
    //include('plugins/colorBox/jquery.colorbox-min.js');
    //include ('main.js');

ob_end_flush();